package com.example.mobileassignment;


import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment3 extends Fragment {



    public Fragment3() {
    }

    private final String ASYNC_TASK_TAG = "ASYNC_TASK";
    private Button executeAsyncTaskButton;
    private Button cancelAsyncTaskButton;
    private ProgressBar asyncTaskProgressBar;
    private TextView asyncTaskLogTextView;
    private MyAsyncTask myAsyncTask;
    private CoordinatorLayout coordinatorLayout;
    private Button button;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_fragment3, container, false);


        this.executeAsyncTaskButton = view.findViewById(R.id.executeAsyncTaskButton);
        this.executeAsyncTaskButton.setEnabled(true);
        this.cancelAsyncTaskButton = view.findViewById(R.id.cancelAsyncTaskButton);
        this.cancelAsyncTaskButton.setEnabled(false);
        this.asyncTaskProgressBar = view.findViewById(R.id.asyncTaskProgressBar);
        this.asyncTaskLogTextView = view.findViewById(R.id.asyncTaskLogTextView);

        executeAsyncTaskButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myAsyncTask = new MyAsyncTask();
                myAsyncTask.execute(Integer.parseInt("10"));
                executeAsyncTaskButton.setEnabled(false);
                cancelAsyncTaskButton.setEnabled(true);
            }
        });

        cancelAsyncTaskButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myAsyncTask.cancel(true);
            }
        });

        coordinatorLayout = view.findViewById(R.id.coordinatorLayout);
        button = view.findViewById(R.id.button);


        return view;
    }



        private class MyAsyncTask extends AsyncTask<Integer, Integer, String> {

            @Override
            protected void onPreExecute() {
                asyncTaskLogTextView.setText("Loading");
                Log.i(ASYNC_TASK_TAG, "onPreExecute() is executed.");
            }
            @Override
            protected String doInBackground(Integer... inputParams) {

                StringBuffer retBuf = new StringBuffer();
                boolean loadComplete = false;

                try
                {
                    Log.i(ASYNC_TASK_TAG, "doInBackground(" + inputParams[0] + ") is invoked.");

                    int paramsLength = inputParams.length;
                    if(paramsLength > 0) {
                        Integer totalNumber = inputParams[0];
                        int totalNumberInt = totalNumber.intValue();

                        for(int i=0;i < totalNumberInt; i++)
                        {
                            int progressValue = (i * 100 ) / totalNumberInt;
                            publishProgress(progressValue);
                            Thread.sleep(200);
                        }

                        loadComplete = true;
                    }
                }catch(Exception ex)
                {
                    Log.i(ASYNC_TASK_TAG, ex.getMessage());
                }finally {
                    if(loadComplete) {
                        retBuf.append("Load is complete.");
                    }else
                    {
                        retBuf.append("Load is canceled.");
                    }
                    return retBuf.toString();
                }
            }
            @Override
            protected void onPostExecute(String result) {
                Log.i(ASYNC_TASK_TAG, "onPostExecute(" + result + ") is invoked.");
                asyncTaskLogTextView.setText(result);
                asyncTaskProgressBar.setProgress(100);
                executeAsyncTaskButton.setEnabled(true);
                cancelAsyncTaskButton.setEnabled(false);
            }





            @Override
            protected void onProgressUpdate(Integer... values) {
                Log.i(ASYNC_TASK_TAG, "onProgressUpdate(" + values + ") is called");
                asyncTaskProgressBar.setProgress(values[0]);
                asyncTaskLogTextView.setText("loading..." + values[0] + "%");
            }
            @Override
            protected void onCancelled(String result) {
                Log.i(ASYNC_TASK_TAG, "onCancelled(" + result + ") is invoked.");
                asyncTaskLogTextView.setText(result);
                asyncTaskProgressBar.setProgress(0);
                executeAsyncTaskButton.setEnabled(true);
                cancelAsyncTaskButton.setEnabled(false);
            }
        }
    }
