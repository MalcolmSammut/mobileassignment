package com.example.mobileassignment;

public class BooksData {
    String isbn;
    String publisher;
    String description;

    public BooksData(String isbn, String publisher, String description)
    {
        this.isbn = isbn;
        this.publisher = publisher;
        this.description = description;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getPublisher() {
        return publisher;
    }

    public String getDescription() {
        return description;
    }
}
