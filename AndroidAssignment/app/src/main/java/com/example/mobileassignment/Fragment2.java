package com.example.mobileassignment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Fragment2 extends Fragment {

    private CoordinatorLayout coordinatorLayout;

    public Fragment2() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_fragment3, container, false);
        coordinatorLayout = view.findViewById(R.id.coordinatorLayout);
        return view;
    }

    Date date = new Date(System.currentTimeMillis());
    @Override
    public void onStop() {
        super.onStop();
        Context context = getActivity();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.getActivity().getWindow().getContext());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
        String strDate = formatter.format(date);
        editor.putString("Time",strDate);
        editor.apply();
    }
    @Override
    public void onPause() {
        super.onPause();
        Context context = getActivity();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.getActivity().getWindow().getContext());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
        String strDate = formatter.format(date);
        editor.putString("Time",strDate);
        editor.apply();
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.getActivity().getWindow().getContext());
        String myDate = sharedPreferences.getString("Time", "default value");
        Snackbar snackbar = Snackbar.make(coordinatorLayout, "The date&time is: "+myDate, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }
    @Override
    public void onStart() {
        super.onStart();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.getActivity().getWindow().getContext());
        String myDate = sharedPreferences.getString("Time", "default value");
        Snackbar snackbar= Snackbar.make(coordinatorLayout, "The date&time is: "+myDate, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }


}
